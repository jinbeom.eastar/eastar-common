import type { Config } from 'tailwindcss';

const theme: Config['theme'] = {
    container: {
        center: true,
        padding: {
            DEFAULT: '1rem',
            '2xl': '3rem'
        },
        screens: {
            '2xl': '1400px'
        }
    },
    colors: {
        primary: {
            DEFAULT: '#d11f26'
        },
        typography: {
            DEFAULT: '#333333'
        }
    }
};

export default theme;
