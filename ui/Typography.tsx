import { cn } from '../lib/utils';
import { PropsWithChildren } from 'react';

export default function Typography({ className, children, variant = 'h1' }: PropsWithChildren<{ className?: string; variant?: 'h1' | 'h2' }>) {
    const Comp = variant;
    return <Comp className={cn(variant === 'h1' && 'text-[32px] font-bold text-typography', variant === 'h2' && '', className)}>{children}</Comp>;
}
